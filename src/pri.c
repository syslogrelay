/* This file is part of syslogrelay.
   Copyright (C) 2022 Sergey Poznyakoff

   Syslogrelay is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Syslogrelay is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with syslogrelay.  If not, see <http://www.gnu.org/licenses/>. */
#include <config.h>
#include <stdlib.h>
#include <sys/uio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sysexits.h>
#include <syslog.h>
#include <syslogrelay.h>

enum {
	PRI_PRIO_NONE,
	PRI_PRIO_KEEP,
	PRI_PRIO_DECODE
};

enum {
	FILE_NAME_OUT,
	FILE_NAME_ERR,
	FILE_NAME_MAX
};

static char *default_file_names[FILE_NAME_MAX] = { "1", "2" };

struct pri_output_channel {
	struct output_channel base;     /* Base output channel. */
	char *file_name[FILE_NAME_MAX]; /* File names. */
	int fd[FILE_NAME_MAX];          /* Corresponding file descriptors. */
	int delim;                      /* Delimiting severity */
	int file_prio;                  /* Priority decode mode. */
};

static struct output_channel *
chan_pri_create(void)
{
	struct pri_output_channel *pchan = xmalloc(sizeof(*pchan));
	pchan->file_name[FILE_NAME_OUT] = pchan->file_name[FILE_NAME_ERR] = NULL;
	pchan->fd[FILE_NAME_OUT] = pchan->fd[FILE_NAME_ERR] = -1;
	pchan->delim = LOG_ERR;
	pchan->file_prio = PRI_PRIO_NONE;
	return (struct output_channel *)pchan;
}

static int
chan_pri_getparam(struct output_channel *chan, char *name, char *value)
{
	struct pri_output_channel *pchan = (struct pri_output_channel *)chan;
	if (strcmp(name, "prio") == 0) {
		if (!value)
			error(EX_USAGE, 0, "prio requires argument");
		if (strcmp(value, "none") == 0)
			pchan->file_prio = PRI_PRIO_NONE;
		else if (strcmp(value, "keep") == 0)
			pchan->file_prio = PRI_PRIO_KEEP;
		else if (strcmp(value, "decode") == 0)
			pchan->file_prio = PRI_PRIO_DECODE;
		else
			error(EX_USAGE, 0, "unrecognized prio argument: %s", value);
	} else if (strcmp(name, "severity") == 0) {
		int n = str_to_severity(value);
		if (n == -1)
			error(EX_USAGE, 0, "not a valid priority name: %s", value);
		pchan->delim = n;
	} else if (strcmp(name, "errfile") == 0) {
		pchan->file_name[FILE_NAME_ERR] = xstrdup(value);
	} else if (strcmp(name, "outfile") == 0) {
		pchan->file_name[FILE_NAME_OUT] = xstrdup(value);
	} else
		return 1;
	return 0;
}

static int
chan_pri_open(struct output_channel *chan)
{
	struct pri_output_channel *pchan = (struct pri_output_channel *)chan;
	int fd;

	if (!chan->name) {
		error(EX_SOFTWARE, 0, "%s:%d: channel name cannot be empty",
		      __FILE__, __LINE__);
	}

	if (pchan->file_name[FILE_NAME_OUT] == NULL)
		pchan->file_name[FILE_NAME_OUT] = xstrdup(default_file_names[FILE_NAME_OUT]);
	if (pchan->file_name[FILE_NAME_ERR] == NULL)
		pchan->file_name[FILE_NAME_ERR] = xstrdup(default_file_names[FILE_NAME_ERR]);

	fd = open(chan->name, O_RDONLY | O_NONBLOCK | O_DIRECTORY);
	if (fd == -1)
		error(EX_UNAVAILABLE, errno, "open %s", chan->name);

	pchan->fd[FILE_NAME_OUT] = openat(fd, pchan->file_name[FILE_NAME_OUT],
				     O_WRONLY | O_APPEND | O_CREAT, 0644);
	if (pchan->fd[FILE_NAME_OUT] == -1)
		error(EX_CANTCREAT, errno, "can't open %s/%s for writing",
		      chan->name, pchan->file_name[FILE_NAME_OUT]);

	pchan->fd[FILE_NAME_ERR] = openat(fd, pchan->file_name[FILE_NAME_ERR],
				     O_WRONLY | O_APPEND | O_CREAT, 0644);
	if (pchan->fd[FILE_NAME_ERR] == -1)
		error(EX_CANTCREAT, errno, "can't open %s/%s for writing",
		      chan->name, pchan->file_name[FILE_NAME_ERR]);

	close(fd);

	return 0;
}

#define IOVCNT 7

static int
chan_pri_send(struct output_channel *chan, struct message *msg)
{
	struct pri_output_channel *pchan = (struct pri_output_channel *)chan;
	struct iovec iov[IOVCNT];
	struct iovec *v = iov;
	int n;
	
	char *buf = msg->buf;
	size_t len = msg->len;

	switch (pchan->file_prio) {
	case PRI_PRIO_NONE:
		buf += msg->ts_start;
		len -= msg->ts_start;
		break;

	case PRI_PRIO_KEEP:
		break;

	case PRI_PRIO_DECODE: {
		char const *f;
		char const *p;

		f = pri_to_facility(msg->pri);
		p = pri_to_severity(msg->pri);
		if (f && p) {
			v->iov_base = "<";
			v->iov_len = 1;
			v++;

			v->iov_base = (char*) f;
			v->iov_len = strlen(f);
			v++;

			v->iov_base = ".";
			v->iov_len = 1;
			v++;

			v->iov_base = (char*) p;
			v->iov_len = strlen(p);
			v++;

			v->iov_base = ">";
			v->iov_len = 1;
			v++;

			buf += msg->ts_start;
			len -= msg->ts_start;
		}
	}
	}

	v->iov_base = (char *) buf;
	v->iov_len = len;
	v++;
	v->iov_base = (char *) "\n";
	v->iov_len = 1;
	v++;

	n = PRI_SEV(msg->pri) <= pchan->delim;
	if (writev(pchan->fd[n], iov, v - iov) < 0) {
		error(0, errno, "error writing to %s/%s", chan->name,
		      pchan->file_name[n]);
		return -1;
	}
	return 0;
}

struct channel_method pri_channel_method = {
	.chan_name = "pri",
	.chan_create = chan_pri_create,
	.chan_open = chan_pri_open,
	.chan_send = chan_pri_send,
	.chan_close = default_chan_close,
	.chan_getparam = chan_pri_getparam
};

	
	
	

