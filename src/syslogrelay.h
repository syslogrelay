/* This file is part of syslogrelay.
 * Copyright (C) 2022 Sergey Poznyakoff
 *
 * Syslogrelay is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Syslogrelay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with syslogrelay.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include "list.h"

#define MAXLINE 1024
#define MESSAGE_QUEUE_MAX 128
#ifndef DEFAULT_CHANNEL
# define DEFAULT_CHANNEL "file:///var/log/messages"
#endif

#define PRI_FAC(p) ((p) >> 3)
#define NUM2FAC(n) ((n) << 3)
#define PRI_SEV(n) ((n) & 0x7)
#define NSTR(t) (sizeof(t)/sizeof(t[0]))

#define	HAS_PRI        0x1
#define	HAS_TIMESTAMP  0x2

struct message {
	int flags;
	int pri;
	int ts_start;         /* Start of the timestamp */
	char *buf;
	size_t len;
	struct list_head link;
};

/* Output channel types. */
enum channel_type {
	CHAN_FILE,  /* Log to file. */
	CHAN_UDP,   /* Relay to a remote collector via UDP. */
	CHAN_TCP,   /* Relay to a remote collector via plain TCP. */
	CHAN_TLS,   /* Relay to a remote collectro via TCP+TLS. */
	CHAN_PRI,   /* Log to either file in a directory, depending on priority */
	CHAN_UNDEF  /* Undefined. Must be last. */
};

struct output_channel {
	enum channel_type type;
	char *name;
	int fd;
	pthread_mutex_t mutex;
};

struct channel_method {
	char const *chan_name;
	struct output_channel *(*chan_create)(void);
	void (*chan_init)(struct output_channel *);
	int (*chan_open)(struct output_channel *);
	int (*chan_send)(struct output_channel *, struct message *);
	void (*chan_close)(struct output_channel *);
	int (*chan_getparam)(struct output_channel *, char *, char *);
	void (*chan_deinit)(struct output_channel *);
};

char const *channel_scheme(struct output_channel *chan);

struct net_output_channel {
	struct output_channel base;
	struct sockaddr *sa;
	socklen_t salen;
	struct addrinfo *bind_addr;
	int connected;
};

struct output_channel *net_output_channel_create(void);
int net_output_open(struct output_channel *chan, int protocol, int socktype, char *defsrv);
void net_output_close(struct output_channel *chan);
int net_output_getparam(struct output_channel *chan, char *name, char *value);


void error(int status, int errnum, char const *fmt, ...);
void enomem(void);
void *xmalloc(size_t size);
char *xstrdup(char *str);

char const *pri_to_facility(int pri);
char const *pri_to_severity(int pri);

int str_to_severity(char const *name);

struct output_channel *default_chan_create(void);
void default_chan_close(struct output_channel *chan);
int tcp_output_open(struct output_channel *chan, char *defservice);

extern struct channel_method udp_channel_method;
extern struct channel_method file_channel_method;
extern struct channel_method tcp_channel_method;
extern struct channel_method tls_channel_method;
extern struct channel_method pri_channel_method;

extern int debug;
