.\" syslogrelay - expand environment variables in input files
.\" Copyright (C) 2022 Sergey Poznyakoff
.\"
.\" Syslogrelay is free software; you can redistribute it and/or modify it
.\" under the terms of the GNU General Public License as published by the
.\" Free Software Foundation; either version 3 of the License, or (at your
.\" option) any later version.
.\"
.\" Syslogrelay is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License along
.\" with syslogrelay. If not, see <http://www.gnu.org/licenses/>.
.TH SYSLOGRELAY 8 "December 24, 2022" "SYSLOGRELAY" "System Manager's Manual"
.SH NAME
syslogrelay \- forward syslog messages to remote server
.SH SYNOPSIS
\fBsyslogrelay\fR\
 [\fB\-Vdhn\fR]\
 [\fB\-H \fIHOSTNAME\fR]\
 [\fB\-O \fIFILE\fR]\
 [\fB\-R \fIHOST\fR[\fB:\fIPORT\fR]]\
 [\fB\-l \fISEVERITY\fR]\
 [\fB\-p \fISOCKET\fR]\
 [\fB\-q \fISIZE\fR]\
 [\fB\-W \fIPARAM\fR]\
 [\fIURL\fR]
.SH DESCRIPTION
\fBSyslogrelay\fR provides system log forwarding facility for confined
environments.  It listens for incoming system log messages on the UNIX
socket file \fB/dev/log\fR and forwards them to the remote server
specified by the \fBURL\fR.
.PP
The URL consists of a scheme, which selects the output \fIchannel\fR
to use, and argument, specifying the actual destination.  The two
parts are separated by \fB://\fR.  Some output channels can take
additional parameters, which can be supplied using the \fB\-W\fR option.
.PP
The following output channels are supported:
.TP
\fBfile://\fIFILE\fR
Write messages to a disk file.  The \fIFILE\fR argument gives the file
name.
.sp
This channel understands the \fBprio=\fR parameter, which controls
whether and how the message priority is reflected on the output.  Its
possible values are:
.RS
.TP
.B none
Priority is not output.  This is the default.
.TP
.B keep
Priority is retained as is, i.e. as a decimal number in angle brackets
at the start of a message.
.TP
.B decode
Priority is printed in human-readable form as syslog facility and
message severity delimited by a single dot and enclosed in angle
brackets.  The facility and severity names used are as described in
.BR syslog (3),
but without the \fBLOG_\fR prefix.
.RE
.TP
\fBpri:///\fIDIR\fR
Distribute messages to two disk files located in directory \fIDIR\fR.
The name of the file to write to is selected depending on the message
severity.  Messages with severity greater than \fBLOG_ERR\fR are directed
to file \fB1\fI (\fIout\fR file), messages with severity less than or equal
to \fILOG_ERR\fR are directed to file \fB2\fR (\fIerr\fR file).
.IP
This channel understands the following parameters:
.RS
.TP
.BI severity= SEVERITY
Sets the name of the delimiting severity.  Messages with severity
less than or equal to that value will be directed to the \fIerr\fR file.
(\fBDIR/2\fR, by default).
.TP
.BI errfile= NAME
Sets the name of the \fIerr\fR file.
.TP
.BI outfile= NAME
Sets the name of the \fIout\fR file.
.TP
\fBprio=\fInone\fR | \fIkeep\fR | \fIdecode\fR
Controls whether and how the message priority is reflected on the
output.  For details, see the description of the channel \fIfile:\fR
above.
.RE
.IP
Default settings correspond to:
.IP
.RS
.EX
\-Wseverity=err \-Werrfile=2 \-Woutfile=1 \-Wprio=none
.EE
.RE
.TP
\fBudp://\fIHOST\fR[\fB:\fIPORT\fR]
Forward messages to the given host using the standard UDP transport
(RFC 5426).  \fIHOST\fR must be an IP address (both IPv4 and IPv6 are
allowed), or a domain name, that resolves to exactly one IP address.
\fIPORT\fR defaults to 514.
.sp
Parameters:
.RS
.TP
.BI bind= IP
Bind socket to the given IP address - either an IPv4 address or IPv6
enclosed in square brackets.
.RE
.TP
\fBtcp://\fIHOST\fR\fB:\fIPORT\fR
Forward messages to the given host using the TCP transport (RFC 6587).
The \fIPORT\fR part is mandatory.  See above for the syntax of
\fIHOST\fR.  This channel implements the \fIOctet Counting\fR message
transfer method.  See \fBudp\fR for a list of available parameters.
.TP
\fBtcp+tls://\fIHOST\fR[\fB:\fIPORT\fR]
Forward messages to the given host using TLS over TCP (RFC 5425).
For the syntax of the \fIHOST\fR part, refer to \fBudp://\fR above.
The default \fIPORT\fR is 6514.
The following parameters can be used with this channel:
.RS
.TP
.BI bind= IP
Bind socket to the given IP address - either an IPv4 address or IPv6
enclosed in square brackets.
.TP
.BI cafile= FILE
Specifies the certificate authority file to use for certificate
verification during the handshake.  Without this parameter,
certificate verification is disabled.
.TP
.BI cert= FILE
Specifies the certificate file for client authentication.  The
\fBkey=\fR parameter must be used as well.
.TP
.BI key= FILE
Specifies the key file for certificate supplied with the \fBcert=\fR
parameter.
.TP
.BI priority= STRING
Specifies the TLS session's handshake algorithms and options to use.
The argument is a GnuTLS priority string as discussed in
<https://gnutls.org/manual/html_node/Priority-Strings.html>.
.RE
.PP
Two shortcuts are implemented to simplify \fIURL\fR syntax in the two most
often used cases.  First, if \fIURL\fR begins with a slash character,
it is assumed to be the name of the local file.  That is,
.PP
.EX
.RS
syslogrelay /WORD
.RE
.EE
.PP
is equivalent to
.PP
.EX
.RS
syslogrelay file:///WORD
.RE
.EE
.PP
Any other URL not starting with a scheme is assumed
to define a \fBudp://\fR channel.  That is, \fBsyslogrelay
127.0.0.1\fR is equivalent to \fBsyslogrelay udp://127.0.0.1\fR.
.PP
Finally, for compatibility with busybox syslog, the \fB\-O\fR and
\fB\-R\fR options are provided.  When either of them is given,
\fIURL\fR may not be used.  The option \fB-O \fIFILE\fR is equivalent
to the URL \fBfile://\fIFILE\fR.  The option \fB\-R \fIHOST\fR is
equivalent to \fBudp://\fIHOST\fR.
.PP
If \fIURL\fR is omitted and neither compatibility option is given,
\fBsyslogrelay\fR assumes \fBfile:///var/log/messages\fR.  This
\fIdefault channel\fR can be changed at compile time.  To be sure,
inspect the output of \fBsyslogrelay -h\fR.
.SS Message queue
The program maintains a \fImessage queue\fR, which is used to
temporarily hold the messages while the selected channel is not able to
receive them.  When the receiver is back online again, the messages from the
queue are transmitted to it.  If the receiver is down for too long,
the queue can get filled up.  In this case, \fBsyslogrelay\fR will
start dropping messages from the head of the queue, i.e. the ones
that sit there for the longest time.  Each dropped message is logged
on the standard error.
.PP
Notice that the above procedure is effective for TCP-based channels
only.
.PP
The default queue size is 128 messages and can be modified using the
\fB\-q\fR command line option.
.SS Message normalization
Before forwarding, received messages are normalized as described
in RFC 3164, section 4.3.  In particular, hostname part is inserted to
messages lacking it.  The hostname to use is determined using the
.BR gethostname (2)
call.  If the result is not satisfactory, it can be overridden with
the \fB\-H\fR option.  Hostname insertion can be disabled using the
\fB\-S\fR option.
.PP
If the message text contains ASCII control characters (ASCII 0 to 31),
these are replaced with their \fIcaret notation\fR, i.e. a caret
character followed by the character value XORed with 0100.  Thus,
horizontal tabulation (ASCII 9) becomes \fB^I\fR, carriage return
(ASCII 13) becomes \fB^M\fR, etc.  By default the newline character is
exempt from this translation.  Instead, it is substituted with
horizontal space (ASCII 32).
.PP
The \fB\-Wcontrol_chars=\fIMODE\fR option can be used to alter this
processing.  Possible values for \fIMODE\fR are:
.TP
.B caret
Replace all control characters with their caret notation.  The newline
character is represented as \fB^J\fR.
.TP
.B caretnl
The default algorithm as described above.
.TP
.B octal
Replace control characters with their octal representation.
.TP
.B raw
Reproduce control characters verbatim.
.SH OPTIONS
.TP
\fB\-H \fIHOSTNAME\fR
Assume \fIHOSTNAME\fR as the current hostname.
.TP
.B \-S
When processing incoming messages, don't insert hostname to the
header, even if it is missing.
.TP
.B \-d
Reserved for future use.
.TP
.B \-h
Displays a short usage summary.
.TP
\fB\-l \fIN\fR
Silently drop messages with the severity value greater than \fIN\fR.
Argument is either a numerical value of the severity (0 through 7), or
one of symbolic names:
.BR emerg ,
.BR alert ,
.BR crit ,
.BR err ,
.BR warning ,
.BR notice ,
.BR info ,
.BR debug
(case-insensitive).
.TP
\fB\-p \fISOCKET\fR
Listen on the given socket file, instead of the default
\fB/dev/log\fR.
.TP
\fB\-q \fIN\fR
Modify the message queue size.
.TP
.B \-V
Print program version and exit.
.TP
\fB\-W \fBPARAM=\fIVALUE\fR
Set the channel-specific parameter.  See \fBDESCRIPTION\fR above for
the list of these.
.SS Compatibility options
The following options are provided for compatibility with
.B busybox
syslog.
.TP
\fB\-O \fIFILE\fR
Write messages to \fIFILE\fR.  Same as specifying \fBfile://\fIFILE\fR
URL as argument.
.TP
\fB\-R \fIHOST\fR[\fB:\fIPORT\fR]
Forward messages to \fIHOST\fR at UDP port \fIPORT\fR.  This is the
same as specifying \fBudp://\fIHOST\fR[\fB:\fIPORT\fR] URL.
.TP
.B -n
Ignored.
.PP
Obviously, it is an error to use \fB\-O\fR or \fB\-R\fR together with
an explicit \fIURL\fR argument.
.SH SEE ALSO
.BR syslog (3),
.BR syslogd (8).
.PP
.BR "RFC 3164" ,
.BR "RFC 5425" ,
.BR "RFC 5426" ,
.BR "RFC 6587" .
.SH BUGS
Multiple UNIX sockets are not supported.  If the \fIHOST\fR part of the
URL argument is a symbolic host name, it must resolve to exactly one
IP address.
.SH COPYRIGHT
Copyright \(co 2022 Sergey Poznyakoff <gray@gnu.org>
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-functions 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
