/* This file is part of syslogrelay.
   Copyright (C) 2022 Sergey Poznyakoff

   Syslogrelay is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Syslogrelay is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with syslogrelay.  If not, see <http://www.gnu.org/licenses/>. */
#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <sysexits.h>
#include <syslogrelay.h>

static int
chan_udp_open(struct output_channel *chan)
{
	return net_output_open(chan, IPPROTO_UDP, SOCK_DGRAM, "514");
}

static int
chan_udp_reconnect(struct net_output_channel *uchan)
{
	if (connect(uchan->base.fd, uchan->sa, uchan->salen) == 0) {
		uchan->connected = 1;
	} else {
		uchan->connected = 0;
		error(0, errno, "can't connect to %s", uchan->base.name);
	}
	return uchan->connected;
}

static int
chan_udp_send(struct output_channel *chan, struct message *msg)
{
	struct net_output_channel *uchan = (struct net_output_channel *)chan;
	int rc;

	if (!uchan->connected) {
		if (!chan_udp_reconnect(uchan))
			return -1;
	}

	rc = send(chan->fd, msg->buf, msg->len, 0);
	if (rc == -1) {
		int ec = errno;

		error(0, errno, "send failed");

		if (ec == ECONNREFUSED || /* connection went down */
		    ec == ENOTCONN ||     /* nobody listening */
		    ec == EDESTADDRREQ || /* BSD equivalents of the above */
		    ec == ECONNRESET) {
			chan_udp_reconnect(uchan);
		}
		return -1;
	} else if (rc != (ssize_t) msg->len) {
		error(0, errno, "sent less bytes than expected (%lu vs. %lu)",
		      (unsigned long) rc, (unsigned long) msg->len);
		return -1;
	}
	return 0;
}

struct channel_method udp_channel_method = {
	.chan_name = "udp",
	.chan_create = net_output_channel_create,
	.chan_open = chan_udp_open,
	.chan_send = chan_udp_send,
	.chan_close = net_output_close,
	.chan_getparam = net_output_getparam
};
