/* This file is part of syslogrelay.
   Copyright (C) 2022 Sergey Poznyakoff

   Syslogrelay is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Syslogrelay is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with syslogrelay.  If not, see <http://www.gnu.org/licenses/>. */
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <sysexits.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include "syslogrelay.h"

char *progname;   /* This program name */
char *hostname;   /* This host name */
size_t hostname_length;
int debug;        /* Debug level */
int max_prio = LOG_DEBUG;  /* Log messages with priorities <= max_prio */
char *logdev = "/dev/log";
size_t buffer_size = MAXLINE;
int skip_hostname = 0;

void
error(int status, int errnum, char const *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "%s: ", progname);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	if (errnum)
		fprintf(stderr, ": %s", strerror(errnum));
	fputc('\n', stderr);
	if (status)
		exit(status);
}

static char const *strfac[] = {
    [PRI_FAC(LOG_USER)] =      "USER",
    [PRI_FAC(LOG_MAIL)] =      "MAIL",
    [PRI_FAC(LOG_DAEMON)] =    "DAEMON",
    [PRI_FAC(LOG_AUTH)] =      "AUTH",
    [PRI_FAC(LOG_AUTHPRIV)] =  "AUTHPRIV",
    [PRI_FAC(LOG_SYSLOG)] =    "SYSLOG",
    [PRI_FAC(LOG_LPR)] =       "LPR",
    [PRI_FAC(LOG_NEWS)] =      "NEWS",
    [PRI_FAC(LOG_UUCP)] =      "UUCP",
    [PRI_FAC(LOG_FTP)] =       "FTP",
    [PRI_FAC(LOG_CRON)] =      "CRON",
    [PRI_FAC(LOG_LOCAL0)] =    "LOCAL0",
    [PRI_FAC(LOG_LOCAL1)] =    "LOCAL1",
    [PRI_FAC(LOG_LOCAL2)] =    "LOCAL2",
    [PRI_FAC(LOG_LOCAL3)] =    "LOCAL3",
    [PRI_FAC(LOG_LOCAL4)] =    "LOCAL4",
    [PRI_FAC(LOG_LOCAL5)] =    "LOCAL5",
    [PRI_FAC(LOG_LOCAL6)] =    "LOCAL6",
    [PRI_FAC(LOG_LOCAL7)] =    "LOCAL7",
};

static char const *strpri[] = {
    [LOG_EMERG] =   "EMERG",
    [LOG_ALERT] =   "ALERT",
    [LOG_CRIT] =    "CRIT",
    [LOG_ERR] =     "ERR",
    [LOG_WARNING] = "WARNING",
    [LOG_NOTICE] =  "NOTICE",
    [LOG_INFO] =    "INFO",
    [LOG_DEBUG] =   "DEBUG",
};

static inline int
kw_to_num(char const *kw, char const **tab, int len)
{
    int i;
    for (i = 0; i < len; i++)
	if (tab[i] && strcasecmp(kw, tab[i]) == 0)
	    return i;
    return -1;
}

static inline char const *
num_to_kw(int n, char const **tab, int len)
{
    if (n < 0 || n > len)
	return NULL;
    return tab[n];
}

char const *
pri_to_facility(int pri)
{
	return num_to_kw(PRI_FAC(pri), strfac, NSTR(strfac));
}

char const *
pri_to_severity(int pri)
{
	return num_to_kw(PRI_SEV(pri), strpri, NSTR(strpri));
}

int
str_to_severity(char const *name)
{
	return kw_to_num(name, strpri, NSTR(strpri));
}

void
enomem(void)
{
	error(EX_OSERR, 0, "out of memory");
}

void *
xmalloc(size_t size)
{
	void *p = malloc(size);
	if (!p)
		enomem();
	return p;
}

char *
xstrdup(char *str)
{
	char *p = strdup(str);
	if (!p)
		enomem();
	return p;
}

#ifndef HOST_NAME_MAX
# define HOST_NAME_MAX 64
#endif

static char *
xgethostname(void)
{
	  char *result = NULL;
	  size_t size = 0;
	  char *p;

	  for (;;) {
		  if (size == 0) {
			  size = HOST_NAME_MAX;
		  } else if ((size_t) -1 / 3 * 2 <= size) {
			  enomem();
		  } else {
			  size += (size + 1) / 2;
		  }
		  p = realloc(result, size);
		  if (!p)
			  enomem();
		  result = p;
		  result[size - 1] = 0;
		  if (gethostname(result, size - 1) == 0) {
			  if (!result[size - 1])
				  break;
		  } else if (errno != 0 && errno != ENAMETOOLONG
			     && errno != EINVAL && errno != ENOMEM)
			  error(EX_OSERR, errno, "can't determine my host name");
	  }

	  /* Try to return fully qualified host name */
	  if (!strchr(result, '.')) {
		  struct addrinfo hints, *rp;

		  memset(&hints, 0, sizeof(hints));
		  hints.ai_family = AF_UNSPEC;
		  hints.ai_flags = AI_CANONNAME;

		  if (getaddrinfo(result, NULL, &hints, &rp) == 0) {
			  free(result);
			  result = xstrdup(rp->ai_canonname);
			  freeaddrinfo(rp);
		  }
	  }
	  return result;
}

static size_t
control_raw_count(unsigned char const *buf, size_t len)
{
	return 0;
}

static void
control_raw_copy(unsigned char *dst, unsigned char *src, size_t len)
{
	memcpy(dst, src, len);
}

static size_t
control_caretnl_count(unsigned char const *buf, size_t len)
{
	size_t n = 0;
	for (; len; len--, buf++)
		if (*buf < 040 && *buf != '\n')
			n++;
	return n;
}

static void
control_caretnl_copy(unsigned char *dst, unsigned char *src, size_t len)
{
	for (; len; len--) {
		if (*src == '\n') {
			*dst++ = ' ';
			src++;
		} else if (*src < 040) {
			*dst++ = '^';
			*dst++ = *src++ ^ 0100;
		} else {
			*dst++ = *src++;
		}
	}
}

static size_t
control_caret_count(unsigned char const *buf, size_t len)
{
	size_t n = 0;
	for (; len; len--, buf++)
		if (*buf < 040)
			n++;
	return n;
}

static void
control_caret_copy(unsigned char *dst, unsigned char *src, size_t len)
{
	for (; len; len--) {
		if (*src < 040) {
			*dst++ = '^';
			*dst++ = *src++ ^ 0100;
		} else {
			*dst++ = *src++;
		}
	}
}

static size_t
control_octal_count(unsigned char const *buf, size_t len)
{
	size_t n = 0;
	for (; len; len--, buf++)
		if (*buf < 040)
			n += 3;
	return n;
}

static void
control_octal_copy(unsigned char *dst, unsigned char *src, size_t len)
{
	for (; len; len--, src++) {
		if (*src < 040) {
			*dst++ = '\\';
			*dst++ = '0';
			*dst++ = ((*src >> 3) & 7) + '0';
			*dst++ = (*src & 7) + '0';
		} else {
			*dst++ = *src;
		}
	}
}

enum control_translation_mode {
	control_raw,
	control_caret,
	control_caretnl,
	control_octal
};

static struct control_translator {
	char *name;
	size_t (*count_fn)(unsigned char const *buf, size_t len);
	void (*copy_fn)(unsigned char *dst, unsigned char *src, size_t len);
} ctrl_trans_table[] = {
	[control_raw] = { "raw", control_raw_count, control_raw_copy },
	[control_caret] = { "caret", control_caret_count, control_caret_copy },
	[control_caretnl] = { "caretnl", control_caretnl_count, control_caretnl_copy },
	[control_octal] = { "octal", control_octal_count, control_octal_copy }
};

static struct control_translator *ctrl_trans = &ctrl_trans_table[control_caretnl];

static int
ctrl_select(char const *name)
{
	int i;
	for (i = 0; i < NSTR(ctrl_trans_table); i++) {
		if (strcmp(ctrl_trans_table[i].name, name) == 0) {
			ctrl_trans = &ctrl_trans_table[i];
			return 0;
		}
	}
	return 1;
}

static size_t
ctrl_count(char const *buf, size_t len)
{
	return ctrl_trans->count_fn((unsigned char *)buf, len);
}

static void
ctrl_copy(char *dst, char *src, size_t len)
{
	return ctrl_trans->copy_fn((unsigned char *)dst, (unsigned char *)src, len);
}

#define TIMESTAMP_LENGTH 15

static int
ismonth(char const *buf, size_t len)
{
	static char *mon[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
			       "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	int i;

	if (len <= 3 || buf[3] != ' ')
		return 0;
	for (i = 0; i < sizeof(mon)/sizeof(mon[0]); i++)
		if (memcmp(mon[i], buf, 3) == 0)
			return 1;
	return 0;
}

struct message *
message_create(char *buf, size_t len)
{
	struct message *msg = calloc(1, sizeof(*msg));
	if (msg) {
		size_t n = ctrl_count(buf, len);
		if ((msg->buf = malloc(len + n)) == NULL) {
			free(msg);
			msg = NULL;
		} else {
			ctrl_copy(msg->buf, buf, len);
			msg->len = len + n;
		}
	}
	return msg;
}

void
message_free(struct message *msg)
{
	free(msg->buf);
	free(msg);
}

void
message_parse(struct message *msg)
{
	char *p;
	unsigned long n;
	int i;
	char *buf = msg->buf;
	size_t len = msg->len;

	if (len < 5 || *buf != '<')
		return;

	errno = 0;
	n = strtoul(buf + 1, &p, 10);
	if (errno)
		return;

	if ((p - buf) > 5 || *p != '>')
		return;

	msg->flags = HAS_PRI;
	msg->pri = n;

	/* Skip past the timestamp part */
	msg->ts_start = i = p - buf + 1;
	if (!ismonth(buf + i, len - i))
		return;
	i += 4;

	/* Skip the day of month */
	if (len - i > 3
	    && (buf[i] == ' ' || isdigit(buf[i])) && isdigit(buf[i+1])
	    && buf[i+2] == ' ')
		i += 3;
	else
		return;

	/* Skip HH:MM:SS */
	if (len - i > 9
	    && isdigit(buf[i])
	    && isdigit(buf[i+1])
	    && buf[i+2] == ':'
	    && isdigit(buf[i+3])
	    && isdigit(buf[i+4])
	    && buf[i+5] == ':'
	    && isdigit(buf[i+6])
	    && isdigit(buf[i+7])
	    && buf[i+8] == ' ')
		i += 9;
	else
		return;

	msg->flags |= HAS_TIMESTAMP;
}

void
message_insert(struct message *msg, char const *text, size_t text_len, size_t off)
{
	size_t len;
	char *buf;

	len = msg->len + text_len;
	buf = realloc(msg->buf, len);
	if (buf) {
		msg->buf = buf;
		msg->len = len;
	} else {
		error(0, 0, "out of memory when modifying message; message truncated");
	}
	memmove(msg->buf + off + text_len, msg->buf + off, msg->len - off - text_len);
	memcpy(msg->buf + off, text, text_len);
}

void
message_normalize(struct message *msg)
{
	int add_hostname;

	if (!(msg->flags & HAS_PRI)) {
		static char default_pri[] = "<13> ";
		static int default_pri_len = sizeof(default_pri) - 1;
		message_insert(msg, default_pri, default_pri_len, 0);
		msg->pri = 13;
		msg->ts_start = default_pri_len;
	}
	if (!(msg->flags & HAS_TIMESTAMP)) {
		struct timeval tv;
		struct tm *tm;
		char ts[TIMESTAMP_LENGTH+2];

		gettimeofday(&tv, NULL);
		tm = localtime(&tv.tv_sec);
		strftime(ts, sizeof(ts), "%b %e %T ", tm);
		message_insert(msg, ts, TIMESTAMP_LENGTH+1, msg->ts_start);
		add_hostname = 1;
	} else {
		add_hostname = (msg->len > hostname_length &&
				!(strncasecmp(msg->buf + msg->ts_start + TIMESTAMP_LENGTH + 1,
					      hostname, hostname_length) == 0 &&
				  isspace (msg->buf[msg->ts_start + TIMESTAMP_LENGTH + 1 + hostname_length])));
	}

	if (add_hostname && !skip_hostname)
		message_insert(msg, hostname, hostname_length + 1,
			       msg->ts_start + TIMESTAMP_LENGTH + 1);
}

static int
open_unix_socket(const char *path)
{
	int fd;
	struct sockaddr_un sun;

	if (strlen(path) >= sizeof(sun.sun_path))
		error(EX_UNAVAILABLE, 0, "UNIX socket name too long: %s", path);

	unlink(path);

	memset(&sun, 0, sizeof(sun));
	sun.sun_family = AF_UNIX;
	strncpy(sun.sun_path, path, sizeof(sun.sun_path) - 1);

	fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd < 0)
		error(EX_UNAVAILABLE, errno, "can't create unix socket");

	if (bind(fd, (struct sockaddr *) &sun, sizeof(sun)))
		error(EX_UNAVAILABLE, errno, "can't bind to %s", path);

	if (chmod(path, 0666))
		error(EX_UNAVAILABLE, errno, "can't chmod %s", path);

	return fd;
}

struct output_channel *
default_chan_create(void)
{
	return xmalloc(sizeof(struct output_channel));
}

void
default_chan_close(struct output_channel *chan)
{
	close(chan->fd);
}

struct output_channel *
net_output_channel_create(void)
{
	struct net_output_channel *chan = calloc(1, sizeof(*chan));
	if (!chan)
		enomem();
	return (struct output_channel *) chan;
}

static void
net_output_bind(struct output_channel *chan, char const *arg)
{
	struct net_output_channel *uchan = (struct net_output_channel *) chan;
	struct addrinfo hints, *rp;
	size_t len = strlen(arg);
	char *node;
	int rc;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_flags = AI_PASSIVE|AI_NUMERICHOST;

	if (len > 2 && arg[0] == '[' && arg[len-1] == ']') {
		hints.ai_family = AF_INET6;
		arg++;
		len -= 2;
	} else {
		hints.ai_family = AF_INET;
	}

	node = xmalloc(len + 1);
	memcpy(node, arg, len);
	node[len] = 0;

	rc = getaddrinfo(node, NULL, &hints, &rp);
	free(node);
	if (rc)
		error(EX_UNAVAILABLE, 0, "can't resolve %s: %s", arg, gai_strerror(rc));
	if (uchan->bind_addr)
		freeaddrinfo(uchan->bind_addr);
	uchan->bind_addr = rp;
}

int
net_output_getparam(struct output_channel *chan, char *name, char *value)
{
	if (strcmp(name, "bind") == 0) {
		if (!value)
			error(EX_USAGE, 0, "%s requires argument", name);
		net_output_bind(chan, value);
		return 0;
	}
	return 1;
}

int
net_output_open(struct output_channel *chan, int protocol, int socktype, char *defsrv)
{
	struct net_output_channel *uchan = (struct net_output_channel *)chan;
	int fd;
	struct addrinfo hints, *rp;
	char *p, *node, *service;
	int rc;
	size_t n;

	if (!chan->name) {
		error(EX_SOFTWARE, 0, "%s:%d: channel name cannot be empty",
		      __FILE__, __LINE__);
	}

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = socktype;
	hints.ai_protocol = protocol;

	if (chan->name[0] == '[' && chan->name[n = strcspn(chan->name + 1, "[]")]) {
		node = xmalloc(n + 1);
		memmove(node, chan->name + 1, n);
		node[n] = 0;
		hints.ai_family = AF_INET6;
		p = strrchr(chan->name + n + 1, ':');
	} else {
		p = strrchr(chan->name, ':');
		if (p) {
			n = p - chan->name;

			if (n == 0)
				error(EX_USAGE, 0, "invalid remote address: %s", chan->name);
			node = xmalloc(n + 1);
			memcpy(node, chan->name, n);
			node[n] = 0;
		} else {
			node = strdup(chan->name);
		}
	}

	if (p) {
		service = xstrdup(p + 1);
	} else if (defsrv) {
		service = xstrdup(defsrv);
		hints.ai_flags |= AI_NUMERICSERV;
	} else {
		error(EX_USAGE, 0, "%s scheme requires explicit port number",
		      channel_scheme(chan));
	}

	rc = getaddrinfo(node, service, &hints, &rp);
	free(node);
	free(service);
	if (rc)
		error(EX_UNAVAILABLE, 0, "can't resolve %s: %s", chan->name, gai_strerror(rc));

	if (rp->ai_next)
		error(EX_USAGE, 0, "%s resolves to multiple addresses", chan->name);
	fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
	if (fd < 0)
		error(EX_OSERR, errno, "can't create socket %s", chan->name);
	chan->fd = fd;
	uchan->sa = xmalloc(rp->ai_addrlen);
	memcpy(uchan->sa, rp->ai_addr, rp->ai_addrlen);
	uchan->salen = rp->ai_addrlen;
	freeaddrinfo(rp);

	if (uchan->bind_addr) {
		if (bind(chan->fd, uchan->bind_addr->ai_addr, uchan->bind_addr->ai_addrlen)) {
			error(0, errno, "bind");
		}
	}

	if (connect(chan->fd, uchan->sa, uchan->salen) == 0) {
		uchan->connected = 1;
	} else {
		uchan->connected = 0;
		error(0, errno, "can't connect to %s", chan->name);
	}

	return 0;
}

void
net_output_close(struct output_channel *chan)
{
	struct net_output_channel *uchan = (struct net_output_channel *)chan;
	close(chan->fd);
	chan->fd = -1;
	free(uchan->sa);
	uchan->sa = NULL;
	uchan->salen = 0;
	if (uchan->bind_addr)
		freeaddrinfo(uchan->bind_addr);
	uchan->bind_addr = NULL;
	uchan->connected = 0;
}

static struct channel_method *channel_method[] = {
	[CHAN_FILE] = &file_channel_method,
	[CHAN_UDP] = &udp_channel_method,
	[CHAN_TCP] = &tcp_channel_method,
#ifdef HAVE_LIBGNUTLS
	[CHAN_TLS] = &tls_channel_method,
#endif
	[CHAN_PRI] = &pri_channel_method,
	[CHAN_UNDEF] = NULL
};

char const *
channel_scheme(struct output_channel *chan)
{
	return channel_method[chan->type]->chan_name;
}

void
channel_parse_param(struct output_channel *chan, char *name, char *value)
{
	if (channel_method[chan->type] &&
	    channel_method[chan->type]->chan_getparam &&
	    channel_method[chan->type]->chan_getparam(chan, name, value) == 0)
		return;
	if (value)
		error(EX_USAGE, 0, "unknown or unsupported channel parameter %s=%s", name, value);
	else
		error(EX_USAGE, 0, "unknown or unsupported channel parameter %s", name);
}

struct channel_param {
	char *name;
	char *value;
	struct list_head link;
};

struct output_channel *
channel_open_type(int type, char *name, struct list_head *param_head)
{
	struct output_channel *chan;
	struct channel_param *param;

	chan = channel_method[type]->chan_create();
	chan->type = type;
	chan->name = name ? xstrdup(name) : NULL;
	chan->fd = -1;
	pthread_mutex_init(&chan->mutex, NULL);
	pthread_mutex_lock(&chan->mutex);

	LIST_FOREACH(param, param_head, link) {
		channel_parse_param(chan, param->name, param->value);
	}

	if (channel_method[type]->chan_init)
		channel_method[type]->chan_init(chan);

	if (channel_method[type]->chan_open(chan) == -1)
		error(EX_UNAVAILABLE, 0, "can't open output channel");
	pthread_mutex_unlock(&chan->mutex);
	return chan;
}

struct output_channel *
channel_open(int type, char *name, struct list_head *param_head)
{
	int n;

	if (type != CHAN_UNDEF)
		return channel_open_type(type, name, param_head);

	if (name == NULL) {
		name = DEFAULT_CHANNEL;
	}

	n = strcspn(name, ":");
	if (strncmp(name + n, "://", 3) == 0) {
		int i;

		for (i = 0; i < CHAN_UNDEF; i++) {
			if (channel_method[i] &&
			    strlen(channel_method[i]->chan_name) == n &&
			    memcmp(channel_method[i]->chan_name, name, n) == 0) {
				return channel_open_type(i, name + n + 3, param_head);
			}
		}
		error(EX_USAGE, 0, "unrecognized remote scheme");
	} else if (name[0] == '/') {
		return channel_open_type(CHAN_FILE, name, param_head);
	} else {
		return channel_open_type(CHAN_UDP, name, param_head);
	}
	return NULL;
}

int
channel_reopen(struct output_channel *chan)
{
	int rc;
	pthread_mutex_lock(&chan->mutex);
	channel_method[chan->type]->chan_close(chan);
	rc = channel_method[chan->type]->chan_open(chan);
	if (rc == -1)
		error(0, 0, "can't open output channel");
	pthread_mutex_unlock(&chan->mutex);
	return rc;
}

int
channel_send(struct output_channel *chan, struct message *msg)
{
	int rc;
	pthread_mutex_lock(&chan->mutex);
	rc = channel_method[chan->type]->chan_send(chan, msg);
	pthread_mutex_unlock(&chan->mutex);
	return rc;
}

void
channel_close(struct output_channel *chan)
{
	pthread_mutex_lock(&chan->mutex);
	channel_method[chan->type]->chan_close(chan);
	if (channel_method[chan->type]->chan_deinit)
		channel_method[chan->type]->chan_deinit(chan);
	free(chan->name);
	pthread_mutex_unlock(&chan->mutex);
	free(chan);
}

static pthread_mutex_t message_queue_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t message_queue_cond = PTHREAD_COND_INITIALIZER;

static struct list_head message_queue = LIST_HEAD_INITIALIZER(message_queue);
static size_t message_queue_size;
static size_t message_queue_max = MESSAGE_QUEUE_MAX;

static struct message *
message_dequeue_unlocked(void)
{
	struct message *msg = LIST_HEAD_DEQUEUE(&message_queue, msg, link);
	if (msg)
		message_queue_size--;
	return msg;
}

static struct message *
message_dequeue(int drain)
{
	struct message *msg;

	pthread_mutex_lock(&message_queue_mutex);
	if (!drain)
		pthread_cond_wait(&message_queue_cond, &message_queue_mutex);
	msg = message_dequeue_unlocked();
	pthread_mutex_unlock(&message_queue_mutex);
	return msg;
}

static void
message_putback(struct message *msg)
{
	pthread_mutex_lock(&message_queue_mutex);
	LIST_HEAD_INSERT_FIRST(&message_queue, msg, link);
	message_queue_size++;
	pthread_mutex_unlock(&message_queue_mutex);
}

static void
message_enqueue(struct message *msg)
{
	pthread_mutex_lock(&message_queue_mutex);

	if (message_queue_size == message_queue_max) {
		struct message *old = message_dequeue_unlocked();
		error(0, 0, "dropping message: %*.*s",
		      (int)old->len, (int)old->len, old->buf);
		message_free(old);
	}

	LIST_HEAD_ENQUEUE(&message_queue, msg, link);
	message_queue_size++;

	pthread_cond_broadcast(&message_queue_cond);
	pthread_mutex_unlock(&message_queue_mutex);
}

void *
thr_writer(void *ptr)
{
	struct output_channel *channel = ptr;
	struct message *msg;

	for (;;) {
		if ((msg = message_dequeue(0)) != NULL) {
			do {
				if (channel_send(channel, msg) == 0) {
					message_free(msg);
				} else {
					message_putback(msg);
					break;
				}
			} while ((msg = message_dequeue(1)) != NULL);
		}
	}
	return NULL;
}

void *
thr_reader(void *ptr)
{
	int fd = *(int*)ptr;
	char *buffer;

	buffer = xmalloc(buffer_size);
	pthread_cleanup_push(free, buffer);

	for (;;) {
		ssize_t len;
		struct message *msg;

		len = recv(fd, buffer, buffer_size, 0);
		if (len < 0) {
			error(0, errno, "%s", "recv");
			continue;
		}
		if (len == 0) {
			error(0, 0, "ignoring zero-length packet");
			continue;
		}

		msg = message_create(buffer, len);
		message_parse(msg);
		if (PRI_SEV(msg->pri) <= max_prio) {
			message_normalize(msg);
			message_enqueue(msg);
		} else
			message_free(msg);
	}

	pthread_cleanup_pop(1);
}

static void
usage(void)
{
	printf("usage: %s [OPTIONS] [URL]\n", progname);
	printf("Relays syslog messages to a remote server or to a file.\n");
	printf("\nOPTIONS are:\n");
	printf("\nMessage modification:\n");
	printf("    -H HOSTNAME     use this hostname\n");
	printf("    -S              don't add hostname to the message\n");
	printf("    -W control_chars=V\n");
	printf("                    handling of ASCII control characters; possible values:\n");
	printf("                     raw - copy verbatim,\n");
	printf("                     caret - use caret notation,\n");
	printf("                     caretnl - use caret notation, except for newline\n");
	printf("                               (ASCII 10), which is replaced with single space\n");
	printf("                               (default),\n");
	printf("                     octal - replace with octal representation\n");

	printf("\nOther options:\n");
	printf("    -d              increase debug verbosity\n");
	printf("    -h              print this help list\n");
	printf("    -l SEVERITY     skip messages with severities greater than the given one\n");
	printf("    -p SOCKET       listen on this UNIX socket (default: /dev/log)\n");
	printf("    -q N            set message queue size (default: %d)\n",
	       MESSAGE_QUEUE_MAX);
	printf("    -s N            set message buffer size (default: %d)\n",
	       MAXLINE);
	printf("    -V              print program version and short license\n");
	printf("    -W PARAM        set channel-specific parameter (see the docs)\n");

	printf("\nCompatibility options:\n");
	printf("    -n              ignored\n");
	printf("    -O FILE         same as '%s file://FILE' (store messages to FILE)\n", progname);
	printf("    -R HOST[:PORT]  same as '%s udp://HOST[:PORT]' (relay messages,\n"
	       "                    PORT defaults to 514)\n", progname);

	printf("\n");
	printf("Default output channel (used in absense of URL, -O, or -R) is:\n %s\n",
	       DEFAULT_CHANNEL);

	printf("\n");
	printf("Report bugs to <%s>.\n", PACKAGE_BUGREPORT);
	printf("Syslogrelay home page: <%s>.\n", PACKAGE_URL);

	printf("\n");
}

void
version(void)
{
    printf("%s (%s) %s\n", progname, PACKAGE_NAME, PACKAGE_VERSION);
    printf("Copyright (C) 2022 Sergey Poznyakoff\n");
    printf("\
\n\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
\n\
");
}

static void
signull(int sig)
{
}

static size_t
getsize(char const *arg, char *descr)
{
	unsigned long n;
	char *p;

	errno = 0;
	n = strtoul(arg, &p, 10);
	if (errno || n == 0 || *p)
		error(EX_USAGE, 0, "invalid %s: %s", descr, arg);
	return n;
}

int
main(int argc, char **argv)
{
	int c;
	int fd;
	int i;
	int chtype = CHAN_UNDEF;
	char *chname = NULL;
	struct output_channel *channel;
	struct sigaction act;
	sigset_t sigs;
	pthread_attr_t attr;
	pthread_t reader_tid, writer_tid;
	static int signals[] = {
		SIGHUP,
		SIGINT,
		SIGQUIT,
		SIGTERM,
		SIGPIPE,
		0
	};

	struct list_head param_head = LIST_HEAD_INITIALIZER(param_head);
	struct channel_param *param, *ptmp;

	progname = strrchr(argv[0], '/');
	if (progname)
		++progname;
	else
		progname = argv[0];

	while ((c = getopt(argc, argv, "H:O:SR:VW:dhl:np:q:s:")) != EOF) {
		switch (c) {
		case 'H':
			hostname = xstrdup(optarg);
			break;

		case 'O':
			chtype = CHAN_FILE;
			chname = optarg;
			break;

		case 'S':
			skip_hostname = 1;
			break;

		case 'R':
			chname = optarg;
			break;

		case 'd':
			debug++;
			break;

		case 'l':
			max_prio = kw_to_num(optarg, strpri, NSTR(strpri));
			if (max_prio == -1) {
				long n;
				char *p;

				errno = 0;
				n = strtol(optarg, &p, 10);
				if (errno || n < 0 || n > LOG_DEBUG)
					error(EX_USAGE, errno, "not a valid number: %s", optarg);
				else if (*p)
					error(EX_USAGE, 0, "not a valid number or priority name");
				max_prio = n;
			}
			break;

		case 'h':
			usage();
			exit(0);

		case 'n':
			/* ignored */
			break;

		case 'p':
			logdev = optarg;
			break;

		case 'q':
			message_queue_max = getsize(optarg, "queue size");
			break;

		case 's':
			buffer_size = getsize(optarg, "buffer size");
			break;

		case 'V':
			version();
			exit(0);

		case 'W':
			param = xmalloc(sizeof(*param) + strlen(optarg) + 1);
			param->name = (char*)(param + 1);
			strcpy(param->name, optarg);
			if ((param->value = strchr(param->name, '=')) != NULL)
				*param->value++ = 0;
			LIST_HEAD_ENQUEUE(&param_head, param, link);
			break;

		default:
			exit(EX_USAGE);
		}
	}

	if (chname == NULL) {
		if (optind < argc) {
			chname = argv[optind++];
		}
	}
	if (argc > optind)
		error(EX_USAGE, 0, "too many arguments; try %s -h for help", progname);

	/* Select control characters representation */
	LIST_FOREACH_SAFE(param, ptmp, &param_head, link) {
		if (strcmp(param->name, "control_chars") == 0) {
			if (!param->value)
				error(EX_USAGE, 0, "%s", "-Wcontrol_chars requires argument");
			if (ctrl_select(param->value))
				error(EX_USAGE, 0, "%s", "unrecongized argument to -Wcontrol_chars");
			LIST_REMOVE(param, link);
			free(param);
			break;
		}
	}

	if (!hostname)
		hostname = xgethostname();
	hostname_length = strlen(hostname);
	hostname = realloc(hostname, hostname_length + 2);
	if (!hostname)
		enomem();
	hostname[hostname_length] = ' ';
	hostname[hostname_length+1] = 0;

	/* Set up signal handler and temporarily block signals. */
	act.sa_flags = 0;
	act.sa_handler = signull;
	sigemptyset(&act.sa_mask);

	sigemptyset(&sigs);

	for (i = 0; signals[i]; i++) {
		sigaddset(&sigs, signals[i]);
		sigaction(signals[i], &act, NULL);
	}

	pthread_sigmask(SIG_BLOCK, &sigs, NULL);

	fd = open_unix_socket(logdev);
	channel = channel_open(chtype, chname, &param_head);

	/* Create threads */
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(&reader_tid, &attr, thr_reader, &fd);
	pthread_create(&writer_tid, &attr, thr_writer, channel);
	pthread_attr_destroy(&attr);

	/* Wait for a signal to arrive */
	pthread_sigmask(SIG_UNBLOCK, &sigs, NULL);
	for (;;) {
		sigwait(&sigs, &i);
		if (i == SIGHUP || i == SIGPIPE) {
			channel_reopen(channel);
		} else
			break;
	}

	/* Stop the threads, */
	pthread_cancel(reader_tid);
	close(fd);

	pthread_cancel(writer_tid);
	channel_close(channel);

	while ((param = LIST_HEAD_DEQUEUE(&param_head, param, link)) != NULL) {
		free(param);
	}

	return 0;
}
