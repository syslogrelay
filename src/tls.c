/* This file is part of syslogrelay.
   Copyright (C) 2022 Sergey Poznyakoff

   Syslogrelay is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Syslogrelay is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with syslogrelay.  If not, see <http://www.gnu.org/licenses/>. */
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <netdb.h>
#include <string.h>
#include <sysexits.h>
#include <syslogrelay.h>
#include <gnutls/gnutls.h>

struct tls_output_channel {
	struct net_output_channel net;
	gnutls_certificate_client_credentials cred;
	gnutls_session_t session;
	char *tls_cert;			/* TLS certificate */
	char *tls_key;			/* TLS key */
	char *tls_cafile;
	char *priority;
};

struct tlsparam {
	char *name;
	size_t off;
};

static struct tlsparam tlsparam[] = {
	{ "cert", offsetof(struct tls_output_channel, tls_cert) },
	{ "key", offsetof(struct tls_output_channel, tls_key) },
	{ "cafile", offsetof(struct tls_output_channel, tls_cafile) },
	{ "priority", offsetof(struct tls_output_channel, priority) },
	{ NULL }
};

static int
chan_tls_getparam(struct output_channel *chan, char *name, char *value)
{
	struct tlsparam *tp;

	if (net_output_getparam(chan, name, value) == 0)
		return 0;

	for (tp = tlsparam; tp->name; tp++) {
		if (strcmp(tp->name, name) == 0) {
			if (!value)
				error(EX_USAGE, 0, "%s requires argument", name);
			*(char**)((char*)chan + tp->off) = xstrdup(value);
			return 0;
		}
	}
	return 1;
}

static struct output_channel *
chan_tls_create(void)
{
	struct tls_output_channel *chan = calloc(1, sizeof(*chan));
	if (!chan)
		enomem();
	return (struct output_channel *) chan;
}

static ssize_t
chan_tls_pull(gnutls_transport_ptr_t fd, void *buf, size_t size)
{
	struct tls_output_channel *uchan = fd;
	int rc;
	do {
		rc = read(uchan->net.base.fd, buf, size);
	} while (rc == -1 && errno == EAGAIN);
	return rc;
}

static ssize_t
chan_tls_push(gnutls_transport_ptr_t fd, const void *buf, size_t size)
{
	struct tls_output_channel *uchan = fd;
	int rc;
	do {
		rc = write(uchan->net.base.fd, buf, size);
	} while (rc == -1 && errno == EAGAIN);
	return rc;
}

static void
verify_certificate(gnutls_session_t session)
{
	int rc;
	unsigned status;

	rc = gnutls_certificate_verify_peers2(session, &status);
	if (rc != GNUTLS_E_SUCCESS)
		error(EX_UNAVAILABLE, 0, "gnutls_certificate_verify_peers2: %s",
		      gnutls_strerror(rc));

	if (status) {
		gnutls_certificate_type_t type = gnutls_certificate_type_get(session);
		gnutls_datum_t out;

		rc = gnutls_certificate_verification_status_print(status, type, &out, 0);
		if (rc < 0) {
			error(0, 0, "certificate is not trusted");
			error(EX_UNAVAILABLE, 0, "additionally, an error was encountered while trying to print verification status: %s", gnutls_strerror(rc));
		}
		error(EX_UNAVAILABLE, 0, "%s", out.data);
	}

#if 0
	if (gnutls_certificate_type_get(session) == GNUTLS_CRT_X509) {
		const gnutls_datum_t *cert_list;
		unsigned int cert_list_size;
		cert_list = gnutls_certificate_get_peers(session, &cert_list_size);
		if (!cert_list)
			error(EX_UNAVAILABLE, 0, "no certificates found");
	}
#endif
}

static int
chan_tls_open(struct output_channel *chan)
{
	struct tls_output_channel *uchan = (struct tls_output_channel *)chan;
	int rc;

	if (net_output_open(chan, IPPROTO_TCP, SOCK_STREAM, "6514"))
		return -1;

	gnutls_init(&uchan->session, GNUTLS_CLIENT);
	gnutls_priority_set_direct(uchan->session,
				   uchan->priority ? uchan->priority : "NORMAL",
				   NULL);

	gnutls_certificate_allocate_credentials(&uchan->cred);
	if (uchan->tls_cafile) {
		rc = gnutls_certificate_set_x509_trust_file(uchan->cred,
							    uchan->tls_cafile,
							    GNUTLS_X509_FMT_PEM);
		if (rc == 0)
			error(0, 0, "no certificates read frim %s", uchan->tls_cafile);
		else if (rc < 0) {
			error(EX_OSERR, 0, "can't set cafile: %s: %s", uchan->tls_cafile, gnutls_strerror(rc));
		}
	}
	if (uchan->tls_cert || uchan->tls_key) {
		if (!uchan->tls_cert)
			error(0, 0, "certificate file given without key file");
		else if (!uchan->tls_key)
			error(0, 0, "key file given without certificate file");
		else {
			rc = gnutls_certificate_set_x509_key_file(uchan->cred,
								  uchan->tls_cert, uchan->tls_key,
								  GNUTLS_X509_FMT_PEM);
			if (rc != GNUTLS_E_SUCCESS) {
				error(EX_OSERR, 0, "can't set cert/keyfile: %s", gnutls_strerror(rc));
			}
		}
	}

	gnutls_credentials_set(uchan->session, GNUTLS_CRD_CERTIFICATE, uchan->cred);

	gnutls_transport_set_pull_function(uchan->session, chan_tls_pull);
	gnutls_transport_set_push_function(uchan->session, chan_tls_push);
	gnutls_transport_set_ptr(uchan->session, (gnutls_transport_ptr_t) chan);

	rc = gnutls_handshake(uchan->session);
	if (rc != GNUTLS_E_SUCCESS)
		error(0, 0, "handshake: %s", gnutls_strerror(rc));
	if (uchan->tls_cafile)
		verify_certificate(uchan->session);

	return 0;
}

static void
chan_tls_init(struct output_channel *chan)
{
	gnutls_global_init();
}

static void
chan_tls_deinit(struct output_channel *chan)
{
	struct tls_output_channel *uchan = (struct tls_output_channel *)chan;
	if (uchan->cred) {
		gnutls_certificate_free_credentials(uchan->cred);
		uchan->cred = NULL;
	}
	gnutls_global_deinit();
}

static void
chan_tls_close(struct output_channel *chan)
{
	struct tls_output_channel *uchan = (struct tls_output_channel *)chan;
	if (uchan->session) {
		gnutls_bye(uchan->session, GNUTLS_SHUT_RDWR);
		gnutls_deinit(uchan->session);
		uchan->session = NULL;
	}
	free(uchan->tls_cert);
	free(uchan->tls_key);
	free(uchan->tls_cafile);
	free(uchan->priority);
	net_output_close(chan);
}

static int
tls_full_write(struct tls_output_channel *chan, char *buf, size_t size)
{
	int rc;

	while (size) {
		do
			rc = gnutls_record_send(chan->session, buf, size);
		while (rc == GNUTLS_E_INTERRUPTED || rc == GNUTLS_E_AGAIN);
		if (rc < 0)
			return rc;
		size -= rc;
		buf += rc;
	}
	return GNUTLS_E_SUCCESS;
}

static int
chan_tls_send(struct output_channel *chan, struct message *msg)
{
	struct tls_output_channel *uchan = (struct tls_output_channel *)chan;
	char nbuf[7];
	int rc, n;

	if (!uchan->net.connected) {
		if (chan_tls_open(chan))
			return -1;
	}

	n = snprintf(nbuf, sizeof(nbuf), "%zu ", msg->len);
	if (n >= sizeof(nbuf)) {
		error(0, 0, "message too long (%zu bytes); dropped", msg->len);
		return 0;
	}

	rc = tls_full_write(uchan, nbuf, n);
	if (rc == GNUTLS_E_SUCCESS) {
		rc = tls_full_write(uchan, msg->buf, msg->len);
	}
	if (rc != GNUTLS_E_SUCCESS) {
		error(0, 0, "error sending message: %s", gnutls_strerror(rc));
		chan_tls_close(chan);
		uchan->net.connected = 0;
		return -1;
	}
	return 0;
}


struct channel_method tls_channel_method = {
	.chan_name = "tcp+tls",
	.chan_init = chan_tls_init,
	.chan_deinit = chan_tls_deinit,
	.chan_create = chan_tls_create,
	.chan_open = chan_tls_open,
	.chan_close = chan_tls_close,
	.chan_getparam = chan_tls_getparam,
	.chan_send = chan_tls_send
};
