/* This file is part of syslogrelay.
   Copyright (C) 2022 Sergey Poznyakoff

   Syslogrelay is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Syslogrelay is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with syslogrelay.  If not, see <http://www.gnu.org/licenses/>. */
#include <config.h>
#include <stdlib.h>
#include <sys/uio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sysexits.h>
#include <syslogrelay.h>

enum {
	FILE_PRIO_NONE,
	FILE_PRIO_KEEP,
	FILE_PRIO_DECODE
};

struct file_output_channel {
	struct output_channel base;
	int file_prio;
};

static struct output_channel *
chan_file_create(void)
{
	struct file_output_channel *fchan = xmalloc(sizeof(*fchan));
	fchan->file_prio = FILE_PRIO_NONE;
	return (struct output_channel *)fchan;
}

static int
chan_file_getparam(struct output_channel *chan, char *name, char *value)
{
	struct file_output_channel *fchan = (struct file_output_channel *)chan;
	if (strcmp(name, "prio") == 0) {
		if (!value)
			error(EX_USAGE, 0, "prio requires argument");
		if (strcmp(value, "none") == 0)
			fchan->file_prio = FILE_PRIO_NONE;
		else if (strcmp(value, "keep") == 0)
			fchan->file_prio = FILE_PRIO_KEEP;
		else if (strcmp(value, "decode") == 0)
			fchan->file_prio = FILE_PRIO_DECODE;
		else
			error(EX_USAGE, 0, "unrecognized prio argument: %s", value);
		return 0;
	}
	return 1;
}

static int
chan_file_open(struct output_channel *chan)
{
	int fd;

	if (!chan->name) {
		error(EX_SOFTWARE, 0, "%s:%d: channel name cannot be empty",
		      __FILE__, __LINE__);
	}

	if (strcmp(chan->name, "-") == 0) {
		fd = dup(1);
		if (fd == -1)
			error(EX_CANTCREAT, errno, "can't dup stdout");
	} else {
		fd = open(chan->name, O_WRONLY | O_APPEND | O_CREAT, 0644);
		if (fd == -1)
			error(EX_CANTCREAT, errno, "can't open %s for writing", chan->name);
	}
	chan->fd = fd;
	return fd;
}

#define IOVCNT 7

static int
chan_file_send(struct output_channel *chan, struct message *msg)
{
	struct file_output_channel *fchan = (struct file_output_channel *)chan;
	struct iovec iov[IOVCNT];
	struct iovec *v = iov;

	char *buf = msg->buf;
	size_t len = msg->len;

	switch (fchan->file_prio) {
	case FILE_PRIO_NONE:
		buf += msg->ts_start;
		len -= msg->ts_start;
		break;

	case FILE_PRIO_KEEP:
		break;

	case FILE_PRIO_DECODE: {
		char const *f;
		char const *p;

		f = pri_to_facility(msg->pri);
		p = pri_to_severity(msg->pri);
		if (f && p) {
			v->iov_base = "<";
			v->iov_len = 1;
			v++;

			v->iov_base = (char*) f;
			v->iov_len = strlen(f);
			v++;

			v->iov_base = ".";
			v->iov_len = 1;
			v++;

			v->iov_base = (char*) p;
			v->iov_len = strlen(p);
			v++;

			v->iov_base = ">";
			v->iov_len = 1;
			v++;

			buf += msg->ts_start;
			len -= msg->ts_start;
		}
	}
	}

	v->iov_base = (char *) buf;
	v->iov_len = len;
	v++;
	v->iov_base = (char *) "\n";
	v->iov_len = 1;
	v++;

	if (writev(chan->fd, iov, v - iov) < 0) {
		error(0, errno, "error writing to %s", chan->name);
		return -1;
	}
	return 0;
}

struct channel_method file_channel_method = {
	.chan_name = "file",
	.chan_create = chan_file_create,
	.chan_open = chan_file_open,
	.chan_send = chan_file_send,
	.chan_close = default_chan_close,
	.chan_getparam = chan_file_getparam
};
