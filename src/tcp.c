/* This file is part of syslogrelay.
   Copyright (C) 2022 Sergey Poznyakoff

   Syslogrelay is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Syslogrelay is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with syslogrelay.  If not, see <http://www.gnu.org/licenses/>. */
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <sysexits.h>
#include <sys/uio.h>
#include <syslogrelay.h>

static int
chan_tcp_open(struct output_channel *chan)
{
	return net_output_open(chan, IPPROTO_TCP, SOCK_STREAM, NULL);
}

static int
chan_tcp_reconnect(struct net_output_channel *uchan)
{
	if (connect(uchan->base.fd, uchan->sa, uchan->salen) == 0) {
		uchan->connected = 1;
	} else {
		uchan->connected = 0;
		error(0, errno, "can't connect to %s", uchan->base.name);
	}
	return uchan->connected;
}

#define IOVCNT 3

static int
chan_tcp_send(struct output_channel *chan, struct message *msg)
{
	struct net_output_channel *uchan = (struct net_output_channel *)chan;
	struct iovec iov[IOVCNT];
	struct iovec *v = iov;
	char nbuf[6];
	int n;

	if (!uchan->connected) {
		if (!chan_tcp_reconnect(uchan))
			return -1;
	}

	n = snprintf(nbuf, sizeof(nbuf), "%zu", msg->len);
	if (n >= sizeof(nbuf)) {
		error(0, 0, "message too long (%zu bytes); dropped", msg->len);
		return 0;
	}

	v->iov_base = nbuf;
	v->iov_len = n;
	v++;

	v->iov_base = " ";
	v->iov_len = 1;
	v++;

	v->iov_base = msg->buf;
	v->iov_len = msg->len;
	v++;

	if (writev(chan->fd, iov, v - iov) < 0) {
		error(0, errno, "error writing to %s", chan->name);
		return -1;
	}
	return 0;
}

struct channel_method tcp_channel_method = {
	.chan_name = "tcp",
	.chan_create = net_output_channel_create,
	.chan_open = chan_tcp_open,
	.chan_send = chan_tcp_send,
	.chan_close = net_output_close,
	.chan_getparam = net_output_getparam
};
